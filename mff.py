#Multiscale Frechet Functions with Gaussian weight system
import numpy
import pdb
import math
import scipy.spatial


def mff_1d(pts,data,sigma):
    """calculates the value of the Multiscale Frechet Function (1D) at an array of pts, 
    for scale sigma and data"""
    #calculating the distance^2 from each pt in pts to each element in data
    dist=spatial.distance.cdist(pts,data,'sqeuclidean')
    #calculating gaussian centered at pt at each element in data
    k=numpy.exp(math.pow((-2)*sigma**2,-1)*dist)
    #calculating mff
    return numpy.sum(k*dist,1)*math.pow(data.shape[1],-1)*math.pow((2*math.pi*sigma**2),-0.5)

def mff_2d(pts,data,sigma,size_grid):
    """calculates the value of the Multiscale Frechet Function (2D) at an grid of pts, 
    for scale sigma and data"""
    if size_grid==1:
        pts=numpy.reshape(pts,(pts.shape[0],1))
    #calculating the distance^2 from from each pt in pts to each element in data
    dist=scipy.spatial.distance.cdist(numpy.transpose(pts),numpy.transpose(data),'sqeuclidean')
    #calculating gaussian centered at pt at each element in data
    weights=((2*math.pi*sigma**2)**(-0.5*pts.shape[0])*numpy.exp(dist*math.pow((-2)*sigma*sigma,-1)))**0.5
    #calculating mff
    mat=numpy.sum(dist*weights,1)*math.pow(data.shape[1],-1)
    return numpy.transpose(numpy.reshape(mat,(size_grid,size_grid)))

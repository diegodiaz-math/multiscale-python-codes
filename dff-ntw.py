#Diffusion Frechet Functions - Networks
import numpy
import pdb
import math


def lapl_mat(adjmat):
    """calculates the laplacian matrix from an adjacency matrix"""
    #initializing diagonal matrix
    rows=adjmat.shape[0]
    D=numpy.zeros([rows,rows])
    #filling diagonal matrix
    d_aux=adjmat.sum(0)
    for i in numpy.arange(0,rows):
        D[i,i]=d_aux[i]
    return D-adjmat

def discrete_laplacian_eigen(adjmat):
    """calculates the eigenfunctions and eigenvalues of the discrete laplacian 
    given an adjacency matrix"""
    #creating laplacian matrix
    laplmat=lapl_mat(adjmat)
    #finding eigenvalues and eigenvectors of laplacian matrix
    evalues,evectors=numpy.linalg.eigh(laplmat)
    return evalues, evectors

def diffusion_distance(ind1,ind2,evalues,evectors,sigma):
    """calculates the difussion distances (squared) between 2 nodes on the 
    graph with the discrete laplacian"""
    aux1=(evectors[ind1,:]-evectors[ind2,:])**2
    aux2=numpy.exp(-2*sigma*evalues)
    return numpy.dot(aux1,aux2)

def diff_dist_mat(evalues,evectors,sigma):
    """calculates the diffusion distance matrix between nodes using t for 
    decay parameter of eigenvalues"""
    size=evalues.shape[0]
    dist_mat=numpy.zeros([size,size])
    for i in xrange(size):
        for j in xrange(i+1,size):
            dist_mat[i,j]=diffusion_distance(i,j,evalues,evectors,sigma)
            dist_mat[j,i]=dist_mat[i,j]
    return dist_mat

def dff_ntw(prob,dist_mat):
    """calculates the value of the diffusion Frechet function at each 
    node given a probability vector on the nodes of the 
    respective graph and its diffusion distances"""
    return numpy.dot(dist_mat,prob)

def ct_distance(ind1,ind2,evalues,evectors):
    """calculates the commute-time distance (squared) between 2 nodes 
    on the graph with the discrete laplacian"""
    aux1=(evectors[ind1,:]-evectors[ind2,:])**2
    aux2=numpy.power(evalues,-1)
    return numpy.dot(aux1,aux2)

def ct_dist_mat(evalues,evectors):
    """calculates the (squared) commute-time distance matrix between nodes"""
    size=evalues.shape[0]
    dist_mat=numpy.zeros([size,size])
    for i in xrange(size):
        for j in xrange(i+1,size):
            dist_mat[i,j]=ct_distance(i,j,evalues,evectors)
            dist_mat[j,i]=dist_mat[i,j]
    return dist_mat

def bound(evalues,sigma,w1):
    """calculates the bound betwenn dfvs of two prob distr for certain sigma"""
    trace=numpy.sum(numpy.exp(-2*sigma*evalues))
    return 4*math.pow((trace-1)*math.pow(2*sigma*math.exp(1),-1),0.5)*w1
#Diffusion Frechet Functions - Euclidean Data
import numpy
import pdb
import math
import scipy.spatial


def dff_1d(pts,data,t):
    """gives the value of the Diffusion (Gaussian) Frechet Functions (1D) at an array of pts, for scale t and data """
    #calculating the distance^2 from pt to each element in data
    dist=spatial.distance.cdist(pts,data,'sqeuclidean')
    #calculating diffusion distance at pt at each element in data
    mat=(numpy.ones([dist.shape[0],dist.shape[1]])-numpy.exp(dist*math.pow((-4)*2*t,-1)))*2*math.pow(4*math.pi*2*t,-0.5)
    #calculating dff 
    mat=numpy.sum(mat,1)
    return mat*data.shape[0]**(-1)

def dff_d2(pts,data,t,size_grid):
    """gives the value of the Diffusion (Gaussian) Frechet Functions (2D) at an grid of pts, for scale t and data """
    if size_grid==1:
        pts=numpy.reshape(pts,(pts.shape[0],1))
    #calculating the distance^2 from pt to each element in data
    dist=scipy.spatial.distance.cdist(numpy.transpose(pts),numpy.transpose(data),'sqeuclidean')
    #calculating diffusion distance at pt at each element in data
    weights=(numpy.ones([dist.shape[0],dist.shape[1]])-numpy.exp(dist*math.pow((-4)*2*t,-1)))
    #calculating dff
    mat=numpy.sum(2*(4*math.pi*2*t)**(-0.5*pts.shape[0])*weights,1)
    return numpy.transpose(numpy.reshape(mat,(size_grid,size_grid)))*data.shape[1]**(-1)
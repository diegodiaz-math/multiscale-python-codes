#Covariance Tensor Fields
import numpy
import pdb
import math
import scipy.spatial


def gauss(data,pt,sigma):
    """this function computes the local covariance matrix of data centered at pt with 
    the Gaussian Kernel with scale sigma"""
    y=numpy.zeros([1,pt.shape[0]])
    y[0,:]=pt
    #calculating the distance^2 from pt to each element in data 
    weights=scipy.spatial.distance.cdist(y,data.T,'sqeuclidean')
    #calculating gaussian kernel weights
    weights=((2*math.pi*sigma**2)**(-0.5*x.shape[0])*numpy.exp(weights*math.pow((-2)*sigma*sigma,-1)))**0.5
    #subtracting pt from each data point and multiplyng by weight
    data=data.T
    data-=pt
    data=(data*weights.T).T
    #multyplying data times its transpose and dividing it by the number of samples
    mat=math.pow(data.shape[1],-1)*numpy.dot(data,data.T)
    return mat

def trunc(data,pt,sigma):
    """this function computes the local covariance matrix with of data centered at pt 
    with the Trucation Kernel with scale sigma"""
    y=numpy.zeros([1,x.shape[0]])
    y[0,:]=pt
    #calculating the distance from pt to each element in data 
    weights=scipy.spatial.distance.cdist(y,data.T,'euclidean')
    #calculating truncation kernel weights
    inds1=numpy.where(weights<=sigma)
    inds2=numpy.where(weights>sigma)
    weights[inds1]=1
    weights[inds2]=0
    weights=weights.reshape([weights.shape[1],1])
    #subtracting pt from each data point and multiplyng by weight
    data=data.T
    data-=pt
    data=(data*weights).T
    #multyplying data times its transpose and dividing it by the number of samples
    if inds1[0].shape[0]>0:
        mat=math.pow(data.shape[1],-1)*numpy.dot(data,data.T)
    else:
        mat=0*numpy.dot(data,data.T)
    return mat

def nl_pca(data,pt,sigma,kernel):
    """this function computes the local covariance matrix and its eigenvalues and 
    eigenvectors centered at pt given data, kernel and scale sigma"""
    #finding local covariance matrix with the kernel function
    if kernel=='gauss':
        mat=gauss(data,pt,sigma)
    else:
        mat=trunc(data,pt,sigma)
    #finding eigenvalues and eigenvectors
    evalues,evectors=numpy.linalg.eigh(mat)
    return evalues,evectors,mat
#Manifold Clustering
import numpy
import pdb
import math
import ctf #ctf.py
import scipy.cluster
import scipy.spatial


def distance(pos_data,mat_data,weights):
    """calculates the distance matrix given pos_data (coordinates of pts) and 
    mat_data (local covariance centerered at each pt)"""
    #creating distance matrix
    if weights[0]>0:
        p=scipy.spatial.distance.pdist(numpy.transpose(pos_data),'sqeuclidean')
        p=scipy.spatial.distance.squareform(p)
    #position distance matrix
    else:
        p=numpy.zeros([mat_data.shape[0],mat_data.shape[0]])
    #frobenius distance matrix
    f=numpy.zeros([mat_data.shape[0],mat_data.shape[0]])
    mat_data=mat_data.reshape([mat_data.shape[0],mat_data.shape[1]*mat_data.shape[2]])
    f=scipy.spatial.distance.pdist(mat_data,'sqeuclidean')
    f=scipy.spatial.distance.squareform(f)
    #adding distance matrices according to weights
    dist_mat=(weights[0]*p+weights[1]*f)**0.5
    return dist_mat

def linkage_clust(dist_mat,num,nsubs,aux=0):
    """calculates the single linkage clustering given the dist_mat, num (number of cluster or 
        cut level), and nsubs (number of clusters if known)"""
    #clustering
    link_mat=scipy.cluster.hierarchy.linkage(dist_mat,method='single')
    if num.is_integer()==False or num==0:
        #cutting according to cophenetic average distance
        cut=numpy.mean(scipy.cluster.hierarchy.cophenet(link_mat))
        std=numpy.std(scipy.cluster.hierarchy.cophenet(link_mat))
        classes=scipy.cluster.hierarchy.fcluster(link_mat,cut-num*std,criterion='distance')
    else:
        #cutting according to given number of clusters
        classes=scipy.cluster.hierarchy.fcluster(link_mat,num,criterion='maxclust')
    #clustering outliers 
    #aux=0 for Algorithm 1 (Basic Manifold Clustering) and 
    #aux=1 for Algorithm 2 (Manifold Clustering with known number of clusters)
    if aux==1:
        count_vec=numpy.bincount(classes)
        ind_max=numpy.zeros(nsubs)
        for i in xrange(nsubs):
            ind_max[i]=numpy.argmax(count_vec)
            count_vec[numpy.argmax(count_vec)]=0

        for i in xrange(classes.shape[0]):
            if numpy.where(ind_max==classes[i])[0].shape[0]==0:
                #changing label of ith element to the closest large cluster
                aux_dist=numpy.copy(dist_mat[i,:])
                dist=10000000000
                for j in xrange(nsubs):
                    inds=numpy.where(classes==ind_max[j])[0]
                    dist0=numpy.min(aux_dist[inds])
                    if dist0<dist:
                        dist=dist0
                        label=ind_max[j]
                classes[i]=int(label)

    return classes,link_mat


##################################################################    
# Usage 
'''
data=data #some samples to investigate presence of manifold clusters

weights=input('distance weights? ') #choose weights as wgts=[a,b]
kern='gauss' #type of kernel to use
sigma=input('sigma? ') #size of kernel
num=input('number of clusters or std cut (not an integer)? ')
num=float(num)
mat_vec=numpy.zeros([data.shape[1],3,3])

for i in xrange(data.shape[1]):
    #calculating local cov mat at each data point
    pt=numpy.array((data[0,i],data[1,i],data[2,i]))
    evalues,evectors,mat=ctf.nl_pca(numpy.copy(data),pt,sigma,kern) 
    mat_vec[i,:,:]=numpy.copy(mat)

#clustering points
dist_mat=distance(numpy.copy(data),numpy.copy(mat_vec),weights)
classes,link_mat=linkage_clust(dist_mat,num,3,1)
'''
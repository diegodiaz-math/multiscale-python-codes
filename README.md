# README

This repository contains Python codes for the methods described in the following two publications:

1. Diego H. Diaz Martinez, Washington Mio, and Facundo Memóli. The Shape of Data and Probability Measures. Applied and Computational Harmonic Analysis. https://doi.org/10.1016/j.acha.2018.03.003. 2018

2. Diego H. Diaz Martinez, Christine H. Lee, Peter T. Kim, Washington Mio. Probing the Geometry of Data with Diffusion Fréchet Functions. Applied and Computational Harmonic Analysis. https://doi.org/10.1016/j.acha.2018.01.003. 2018

## Summary
Covariance Tensor Fields (ctf.py) These methods generalize covariance matrices using kernels for obtaining more information on local and regional geometry of random data. The notion of local is modeled with the sigma parameter. They can be used for manifold clustering (manifold.py), shape landmarking, and video motion segmentation tasks to produce meaningful features that could be exploited by other machine learning and AI type algorithms.

Multiscale Frechet Functions (mff.py) These methods generalize the expected variance (Fréchet) function using kernels and diffusion distances for obtaining more information on local/regional variance, number of modes, and organization of random data points. The notion of local is modeled with the sigma parameter. They can be used in 1D and 2D Euclidean data points.

Diffusion Frechet Functions (dff-euc.py, dff-ntw.py) These methods introduce a new notions of distance in Euclidean spaces and weighted networks using diffusion distances that depend on a local parameter. Behaviours of probability distributions defined in these spaces can be analyzed at multiple scales.

## Environment Tested:

a. Python 2.7.6

b. Libraries: numpy 1.11.3, scipy 0.17.1, math


## Usage
```python
import ctf
import dff-euc
import dff-ntw
import manifold
import mff

#Example
mff=mff.mff_2d(pts,data,sigma,size_grid) 
#returns the value of the multiscale frechet function of 2D (data) points at a grid
```